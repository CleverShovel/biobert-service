from os import environ


class Config:
    CELERY_BROKER_URL = environ.get('CELERY_BROKER_URL')
    CELERY_RESULT_BACKEND = environ.get('CELERY_RESULT_BACKEND')

    SQLALCHEMY_DATABASE_URI = environ.get('DATABASE_URL')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    SCRIPT_DIR = environ.get('SCRIPT_DIR')
    BIOBERT_DIR = environ.get('BIOBERT_DIR')

    ARTICLE_DIR = environ.get('ARTICLE_DIR')
    INPUT_DIR = environ.get('INPUT_DIR')
    OUTPUT_DIR = environ.get('OUTPUT_DIR')