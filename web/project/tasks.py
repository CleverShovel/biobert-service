from os import environ, path
from logging import INFO

from celery import Celery
from celery.utils.log import get_task_logger

# from project.celery_app import celery_instance
from utils.subprocess import call


celery_app = Celery(__name__,
                    broker=environ.get('CELERY_BROKER_URL'),
                    backend=environ.get('CELERY_BACKEND_URL'))

logger = get_task_logger(__name__)

@celery_app.task
def launch_biobert(req_id, 
                   script_dir, 
                   biobert_dir,
                   input_dir,
                   output_dir):
    script_path = path.join(script_dir, 'run_list.py')
    vocab = path.join(biobert_dir, 'vocab.txt')
    bert_config = path.join(biobert_dir, 'bert_config.json')
    checkpoint = path.join(biobert_dir, 'model.ckpt-14599')
    in_file = path.join(input_dir, '{}.json'.format(req_id))
    out_dir = path.join(output_dir, req_id)
    command = ['python', 
               script_path,
               '--do_train=False',
               '--do_predict=True',
               '--vocab_file={}'.format(vocab),
               '--bert_config_file={}'.format(bert_config),
               '--init_checkpoint={}'.format(checkpoint),
               '--max_seq_length=384',
               '--doc_stride=128',
               '--n_best_size=5',
               '--do_lower_case=False',
               '--predict_file={}'.format(in_file),
               '--output_dir={}'.format(out_dir)]

    call(command, logger, stdout_log_level=INFO)