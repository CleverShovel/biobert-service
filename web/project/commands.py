import click
from flask import Blueprint

from project.database import db

commands = Blueprint('commands', __name__, cli_group=None)

@commands.cli.command("create-db")
def create_db():
    """Create empty database and tables"""
    db.drop_all()
    db.create_all()
    db.session.commit()