from os import path
import uuid
import json

from flask import request, jsonify
from flask import Blueprint
from flask import current_app
from celery.result import AsyncResult

from project.database import db
from project.tasks import launch_biobert, celery_app
from project.models import Article, Question, Answer


biobert = Blueprint('biobert', __name__)

task_table = {}

@biobert.route('/add-articles', methods=['POST'])
def add_articles():
    # try:
    print(current_app.config['SQLALCHEMY_DATABASE_URI'])
    data = request.get_json()
    articles = []
    for article in data['articles']:
        filename = '{}.json'.format(str(uuid.uuid4()))
        filepath = path.join(current_app.config['ARTICLE_DIR'], filename)
        with open(filepath, 'w') as f:
            json.dump(article['metadata'], f)
        articles.append(Article(name=article['metadata']['name'],
                                text=article['text'], 
                                metadata_filename=filename,
                                metadata_type=article['metadata_type']))
    db.session.bulk_save_objects(articles)
    db.session.commit()
    return jsonify({'status': 'ok'})
    # except Exception as e:
    #     return jsonify({'status': 'error',
    #                     'error_text': str(e)})

def build_ask_request(articles, questions):
    id_format = "{}_{}"
    ask_req = {}
    ask_req['data'] = []
    for article in articles:
        part = {}
        part['title'] = article.id
        part_questions = []
        for q in questions:
            part_questions.append({
                'question': q.text, 
                'id': id_format.format(article.id, q.id)
            })
        part['paragraphs'] = [{
            'context': article.text, 
            'qas': part_questions}]
        ask_req['data'].append(part)
    return ask_req

@biobert.route('/ask', methods=['POST', 'GET'])
def predict():
    # try:
    if request.method == 'POST':
        req_id = str(uuid.uuid4())
        data = request.get_json()
        
        questions = []
        for q in data['questions']:
            questions.append(Question(text=q))
        db.session.bulk_save_objects(questions, return_defaults=True)
        db.session.commit()

        ask_req = build_ask_request(Article.query.all(), questions)
        ask_filename = path.join(current_app.config['INPUT_DIR'], '{}.json'.format(req_id))
        with open(ask_filename, 'w') as f:
            json.dump(ask_req, f)
        task = launch_biobert.delay(req_id, 
                                    current_app.config['SCRIPT_DIR'],
                                    current_app.config['BIOBERT_DIR'],
                                    current_app.config['INPUT_DIR'],
                                    current_app.config['OUTPUT_DIR'])
        task_table[req_id] = task.task_id
        return jsonify({'id': req_id})
    else:
        req_id = request.get_json()['id']
        if req_id not in task_table:
            return jsonify({'status': 'unknown_id'})
        ready = AsyncResult(task_table[req_id], app=celery_app).ready()
        if not ready:
            return jsonify({'status': 'in_process'})
        else:
            out_filename = path.join(
                current_app.config['OUTPUT_DIR'], 
                req_id, 
                'nbest_predictions.json')
            with open(out_filename) as out_f:
                result = json.loads(out_f.read())
            results = []
            answers = []
            for ans_id in result:
                art_id, q_id = map(int, ans_id.split('_'))
                best_ans = result[ans_id][0]
                results.append({
                    'question': best_ans['question_text'],
                    'answer': best_ans['text'],
                    'prob': best_ans['probability']
                })
                answers.append(Answer(
                    article_id=art_id, 
                    question_id=q_id,
                    text=best_ans['text'],
                    prob=best_ans['probability']
                ))
            db.session.bulk_save_objects(answers)
            return jsonify({'status': 'ready', 'results': results})
    # except Exception as e:
    #     return jsonify({'status': 'error', 'error_text': str(e)})
