from os import makedirs, environ

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from project.database import db
from project.biobert import biobert
from project.commands import commands

app = Flask(__name__)
app.config.from_object('project.config.Config')
app.register_blueprint(biobert, url_prefix='/biobert')
app.register_blueprint(commands)
db.init_app(app)

makedirs(app.config['INPUT_DIR'], exist_ok=True)
makedirs(app.config['ARTICLE_DIR'], exist_ok=True)